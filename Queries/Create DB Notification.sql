CREATE TABLE notification_type (
  id INT PRIMARY KEY IDENTITY,
  type VARCHAR(100) NOT NULL,
  created_date DATETIME DEFAULT GETDATE(),
  created_by VARCHAR(100),
  updated_date DATETIME,
  updated_by VARCHAR(100)
);

CREATE TABLE notification_log (
  id INT PRIMARY KEY IDENTITY,
  issue_id INT NOT NULL,
  notification_type INT NOT NULL,
  receiver_address VARCHAR(200),
  user_id INT,
  content VARCHAR(500),
  status INT,
  created_date DATETIME DEFAULT GETDATE(),
  created_by VARCHAR(100), 
  updated_date DATETIME,
  updated_by VARCHAR(100),
  
  FOREIGN KEY(notification_type) 
    REFERENCES notification_type(id)
);