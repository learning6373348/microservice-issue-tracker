use db_master;
-- Insert data project
INSERT INTO project(project_name, created_by) 
VALUES  ("E-Commerce Site", "Admin"),
        ("Payment Gateway", "Admin"),
        ("Mobile Apps", "John"),
        ("ERP System", "Doe");
        
-- Insert data role        
INSERT INTO role(role_name)
VALUES ("Project Manager"),
       ("Programmer"),
       ("Tester");
       
-- Insert data user
INSERT INTO user(user_name, email, phone_number) 
VALUES ("John Wick", "john@example.com","0812831223"),
       ("Sarah Connor", "sarah@example.com","0812837182"),
       ("Bruce Wayne", "bruce@example.com","0812938912");
       
-- Insert data user_role
INSERT INTO user_role(user_id, role_id)
VALUES (1, 1), -- john as manager
       (1, 2), -- john as programmer
       (2, 3), -- sarah as tester
       (3, 2); -- bruce as programmer