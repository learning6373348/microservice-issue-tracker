INSERT INTO issue (project_id, title, description, priority, status, assigned_to, reported_by, date_resolved, created_by, updated_by)
VALUES
    (1, 'Issue 1', 'Deskripsi issue 1', 'High', 0, 0, 3, NULL, 'admin', 'admin'),
    (1, 'Issue 2', 'Deskripsi issue 2', 'Medium', 1, 2, 5, NULL, 'admin', 'admin'),
    (2, 'Issue 3', 'Deskripsi issue 3', 'Low', 0, 0, 3, '2023-01-15', 'admin', 'admin'),
    (2, 'Issue 4', 'Deskripsi issue 4', 'High', 2, 2, 1, NULL, 'admin', 'admin'),
    (3, 'Issue 5', 'Deskripsi issue 5', 'Medium', 1, 2, 3, NULL, 'admin', 'admin');

INSERT INTO issue_comments (issue_id, comment, user_id)
VALUES
    (1, 'Komentar untuk isu 1', 1),
    (1, 'Komentar lain untuk isu 1', 2),
    (2, 'Komentar untuk isu 2', 3),
    (3, 'Komentar untuk isu 3', 3),
    (2, 'Komentar lain untuk isu 2', 2);
