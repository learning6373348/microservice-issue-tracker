-- Insert data notification type
INSERT INTO notification_type (type)
VALUES ('Email'), 
       ('Slack'),
       ('SMS'),
       ('Mobile Push'),
       ('Web Push');
       
-- Insert data notification log      
INSERT INTO notification_log (issue_id, notification_type, receiver_address, user_id, content)
VALUES (1, 1, 'john@email.com', 1, 'New issue created'),
       (2, 2, '@john_slack', 2, 'Issue status updated'),
       (3, 3, '+628123456789', 1, 'New comment posted'),
       (4, 4, 'c5sdf476dfsdf98', 2, 'Issue assigned to you'),
       (5, 5, 'http://onesignal.com/123', 3, 'Priority changed to High');