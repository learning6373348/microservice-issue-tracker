
CREATE TABLE issue (
    issue_id serial PRIMARY KEY,
    project_id INT NOT NULL,
    title VARCHAR(100) NOT NULL,
    description TEXT,
    priority VARCHAR(10),
    status INT,
    assigned_to INT, 
    reported_by INT,
    date_resolved DATE,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_by VARCHAR(50),
    updated_date TIMESTAMP, 
    updated_by VARCHAR(50)
);

CREATE TABLE issue_comments (
    comment_id serial PRIMARY KEY,
    issue_id INT NOT NULL REFERENCES issue(issue_id),
    comment TEXT NOT NULL, 
    user_id INT NOT NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_by VARCHAR(50),
    updated_date TIMESTAMP,
    updated_by VARCHAR(50)
);