use db_master;
CREATE TABLE project (
  project_id INT PRIMARY KEY AUTO_INCREMENT,
  project_name VARCHAR(100) NOT NULL,
  is_active TINYINT DEFAULT 1,
  created_date DATETIME DEFAULT CURRENT_TIMESTAMP,
  created_by VARCHAR(50),
  updated_date DATETIME ON UPDATE CURRENT_TIMESTAMP,
  updated_by VARCHAR(50)  
);

CREATE TABLE role (
  role_id INT PRIMARY KEY AUTO_INCREMENT,
  role_name VARCHAR(50) NOT NULL unique,
  created_date DATETIME DEFAULT CURRENT_TIMESTAMP,
  created_by VARCHAR(50),
  updated_date DATETIME ON UPDATE CURRENT_TIMESTAMP,
  updated_by VARCHAR(50)   
);

CREATE TABLE user (
  user_id INT PRIMARY KEY AUTO_INCREMENT,
  user_name VARCHAR(100) NOT NULL, 
  email VARCHAR(100) NOT NULL unique,
  phone_number VARCHAR(100) NOT NULL unique,  
  created_date DATETIME DEFAULT CURRENT_TIMESTAMP,
  created_by VARCHAR(50),
  updated_date DATETIME ON UPDATE CURRENT_TIMESTAMP,
  updated_by VARCHAR(50)   
);

CREATE TABLE user_role (
  id INT PRIMARY KEY AUTO_INCREMENT,
  user_id INT,
  role_id INT,
  created_date DATETIME DEFAULT CURRENT_TIMESTAMP,
  created_by VARCHAR(50),
  updated_date DATETIME ON UPDATE CURRENT_TIMESTAMP,
  updated_by VARCHAR(50),    
  FOREIGN KEY(user_id) REFERENCES user(user_id),
  FOREIGN KEY(role_id) REFERENCES role(role_id)
);