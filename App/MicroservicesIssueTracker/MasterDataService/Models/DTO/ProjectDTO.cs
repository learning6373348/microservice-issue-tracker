﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MasterDataService.Models.DTO
{
    public class ProjectDTO
    {
        public string? ProjectName { get; set; }
        public bool IsActive { get; set; }
    }
}
