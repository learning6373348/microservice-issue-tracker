﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MasterDataService.Models.DTO
{
    public class UserDTO
    {
        public string? UserName { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
