﻿namespace MasterDataService.Models.DTO
{
    public class UserResponseDTO : User
    {
        public List<RoleDTO> roles { get; set; }
    }
}
