﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MasterDataService.Models
{
    [Table("project")]
    public class Project : BaseEntity
    {
        [Key]
        [Column("project_id")]
        public int ProjectId { get; set; }
        [Column("project_name")]
        public string? ProjectName { get; set; }
        [Column("is_active")]
        public bool IsActive { get; set; }
    }
}
