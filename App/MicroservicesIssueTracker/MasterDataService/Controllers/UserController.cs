﻿using AutoMapper;
using MasterDataService.Models;
using MasterDataService.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace MasterDataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        public UserController(AppDbContext context, IMapper mapper)
        {
            _context=context;
            _mapper=mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<User>> GetAll()
        {
            return _context.Users;
        }

        [HttpGet("{userId:int}")]
        public async Task<ActionResult<UserResponseDTO>> GetById(int userId)
        {
            var user = await _context.Users.FindAsync(userId);
            if (user == null) return NotFound(new { status = 404, message = "User ID Not Found!" });
            var userResponse = _mapper.Map<UserResponseDTO>(user);

            userResponse.roles = _mapper.Map<IEnumerable<RoleDTO>>(_context.UserRoles.Include(x=>x.Role)
                .Where(x=>userId == x.UserId).ToList().Select(x=>x.Role!.RoleName)).ToList();

            return userResponse;
        }

        [HttpPost]
        public async Task<ActionResult> Create(UserDTO userDto)
        {
            var user = _mapper.Map<User>(userDto);
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpPost("AddUserRole")]
        public async Task<ActionResult> AddUserRole(UserRoleDTO userRoleDto)
        {
            var userRole = _mapper.Map<UserRole>(userRoleDto);
            await _context.UserRoles.AddAsync(userRole);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpPut("{userId:int}")]
        public async Task<ActionResult> Update([FromBody] UserDTO userDto, int userId)
        {
            try
            {
                var user = _mapper.Map<User>(userDto);
                user.UserId = userId;
                _context.Users.Update(user);

                if (await _context.Users.FindAsync(userId) == null) return NotFound(new { status = 404, message = "Data Not Found" });
                await _context.SaveChangesAsync();
                return Ok(new { status = 200, message = "Process success!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message.ToString() });
            }
        }

        [HttpDelete("{userId:int}")]
        public async Task<ActionResult> Delete(int userId)
        {
            var user = await _context.Users.FindAsync(userId);
            if (user == null) return NotFound(new { status = 404, message = "User ID Not Found!" });

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpDelete("DeleteUserRole")]
        public async Task<ActionResult> DeleteUserRole(UserRoleDTO userRoleDto)
        {
            var user = await _context.UserRoles.FirstOrDefaultAsync(x => x.UserId == userRoleDto.UserId && x.RoleId == userRoleDto.RoleId);
            if (user == null) return NotFound(new { status = 404, message = "Data Not Found!" });

            _context.UserRoles.Remove(user);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }
    }
}
