﻿using AutoMapper;
using MasterDataService.Models;
using MasterDataService.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        public ProjectController(AppDbContext context, IMapper mapper)
        {
            _context=context;
            _mapper=mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Project>> GetAll()
        {
            return _context.Projects;
        }

        [HttpGet("{projectId:int}")]
        public async Task<ActionResult<Project>> GetById(int projectId)
        {
            var project = await _context.Projects.FindAsync(projectId);
            if (project == null) return NotFound(new { status = 404, message = "Data Not Found!" });
            return project;
        }

        [HttpPost]
        public async Task<ActionResult> Create(ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            await _context.Projects.AddAsync(project);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpPut("{projectId:int}")]
        public async Task<ActionResult> Update([FromBody] ProjectDTO projectDto, int projectId)
        {
            try
            {
                var project = _mapper.Map<Project>(projectDto);
                project.ProjectId = projectId;
                _context.Projects.Update(project);

                if (_context.Projects.Find(projectId) == null) return NotFound(new { status = 404, message = "Data Not Found" });
                await _context.SaveChangesAsync();
                return Ok(new { status = 200, message = "Process success!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message.ToString() });
            }
        }

        [HttpDelete("{projectId:int}")]
        public async Task<ActionResult> Delete(int projectId)
        {
            var project = await _context.Projects.FindAsync(projectId);
            if (project == null) return NotFound(new { status = 404, message = "Data Not Found!" });

            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }
    }
}
