﻿using AutoMapper;
using MasterDataService.Models;
using MasterDataService.Models.DTO;
using System.Reflection;

namespace MasterDataService.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());
            CreateMap<UserDTO, User>()
                .ReverseMap();
            CreateMap<UserRoleDTO, UserRole>();
            CreateMap<ProjectDTO, Project>();
            CreateMap<RoleDTO, Role>();
            CreateMap<String, RoleDTO>()
                .ForMember(d=>d.RoleName, d=>d.MapFrom(x => x));
            CreateMap<User, UserResponseDTO>();

        }
        private void ApplyMappingsFromAssembly(Assembly assembly)
        {
            var types = assembly.GetExportedTypes()
            .Where(t => t.GetInterfaces().Any(i =>
                    i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>)))
                .ToList();

            foreach (var type in types)
            {
                var instance = Activator.CreateInstance(type);

                var methodInfo = type.GetMethod("Mapping")
                    ?? type.GetInterface("IMapFrom`1").GetMethod("Mapping");

                methodInfo?.Invoke(instance, new object[] { this });

            }
        }
    }
}
