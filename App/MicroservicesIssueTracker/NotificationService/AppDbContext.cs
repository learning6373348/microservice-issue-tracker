﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using NotificationService.Models;

namespace NotificationService
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> dbContextOptions) : base(dbContextOptions)
        {
            try
            {
                var dbCreator = Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;
                if (dbCreator != null)
                {
                    if (!dbCreator.CanConnect()) dbCreator.Create();
                    if (!dbCreator.HasTables()) dbCreator.CreateTables();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            const string currentUser = "admin";

            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).CreatedDate = System.DateTime.Now;
                    ((BaseEntity)entityEntry.Entity).CreatedBy = currentUser;
                }
                else
                {
                    Entry((BaseEntity)entityEntry.Entity).Property(p => p.CreatedDate).IsModified = false;
                    Entry((BaseEntity)entityEntry.Entity).Property(p => p.CreatedBy).IsModified = false;
                }
                ((BaseEntity)entityEntry.Entity).UpdatedDate = System.DateTime.Now;
                ((BaseEntity)entityEntry.Entity).UpdatedBy = "admin";
            }

            return await base.SaveChangesAsync(cancellationToken);
        }

        #region DB Set
        public DbSet<NotificationLog> NotificationLogs { get; set; }
        public DbSet<NotificationType> NotificationTypes { get; set; }
        #endregion
    }
}
