﻿namespace NotificationService.Models.DTO
{
    public class NotifTypeDTO
    {
        public string Type { get; set; }
    }
}
