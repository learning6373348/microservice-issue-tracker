﻿namespace NotificationService.Models.DTO
{
    public class NotifDTO
    {
        public int IssueId { get; set; }
        public int NotificationType { get; set; }
        public string ReceiverAddress { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
    }
}
