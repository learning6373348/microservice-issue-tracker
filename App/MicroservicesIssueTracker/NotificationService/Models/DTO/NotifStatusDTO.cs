﻿namespace NotificationService.Models.DTO
{
    public class NotifStatusDTO
    {
        public int Status { get; set; }
    }
}
