﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NotificationService.Models
{
    [Table("notification_log")]
    public class NotificationLog : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }
        [Column("issue_id")]
        public int IssueId { get; set; }
        [Column("notification_type")]
        public int NotificationType { get; set; }
        [Column("receiver_address")]
        public string ReceiverAddress { get; set; }
        [Column("user_id")]
        public int UserId { get; set; }
        [Column("content")]
        public string Content { get; set; }
        [Column("status")]
        public int Status { get; set; } = 0;
    }
}
