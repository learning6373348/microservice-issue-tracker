﻿using AutoMapper;
using IssueTrackerService.Models;
using IssueTrackerService.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IssueTrackerService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IssueController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        public IssueController(AppDbContext context, IMapper mapper)
        {
            _context=context;
            _mapper=mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Issue>> GetAll()
        {
            return _context.Issues;
        }

        [HttpGet("{issueId:int}")]
        public async Task<ActionResult<Issue>> GetById(int issueId)
        {
            var issue = await _context.Issues.FindAsync(issueId);
            if (issue == null) return NotFound(new { status = 404, message = "Data Not Found!" });
            return issue;
        }

        [HttpPost]
        public async Task<ActionResult> Create(IssueDTO issueDto)
        {
            var issue = _mapper.Map<Issue>(issueDto);
            await _context.Issues.AddAsync(issue);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpPut("{issueId:int}")]
        public async Task<ActionResult> Update([FromBody] IssueDTO issueDto, int issueId)
        {
            try
            {
                var issue = _mapper.Map<Issue>(issueDto);
                issue.IssueId = issueId;
                _context.Issues.Update(issue);

                if (_context.Issues.Find(issueId) == null) return NotFound(new { status = 404, message = "Data Not Found" });
                await _context.SaveChangesAsync();
                return Ok(new { status = 200, message = "Process success!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message.ToString() });
            }
        }

        [HttpDelete("{issueId:int}")]
        public async Task<ActionResult> Delete(int issueId)
        {
            var issue = await _context.Issues.FindAsync(issueId);
            if (issue == null) return NotFound(new { status = 404, message = "Data Not Found!" });

            _context.Issues.Remove(issue);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpPut("UpdateIssueStatus/{issueId:int}")]
        public async Task<ActionResult> UpdateIssueStatus([FromBody] IssueStatusDTO issueDto, int issueId)
        {
            try
            {
                var issue = _context.Issues.Find(issueId);
                if (issue == null) return NotFound(new { status = 404, message = "Data Not Found" });

                issue.Status = issueDto.Status;
                _context.Issues.Update(issue);

                await _context.SaveChangesAsync();
                return Ok(new { status = 200, message = "Process success!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message.ToString() });
            }
        }

        [HttpPut("UpdateSetAssignee/{issueId:int}")]
        public async Task<ActionResult> UpdateSetAssignee([FromBody] IssueAssigneeDTO issueDto, int issueId)
        {
            try
            {
                var issue = _context.Issues.Find(issueId);
                if (issue == null) return NotFound(new { status = 404, message = "Data Not Found" });

                issue.AssignedTo = issueDto.AssignedTo;
                _context.Issues.Update(issue);

                await _context.SaveChangesAsync();
                return Ok(new { status = 200, message = "Process success!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message.ToString() });
            }
        }

        #region Issue Comments
        [HttpGet("GetComment")]
        public ActionResult<IssueComListDTO> GetCommentById(int issueId)
        {
            var issueComm = _context.IssueComments.Where(x=>x.IssueId == issueId).ToList();

            return new IssueComListDTO()
            {
                IssueId = issueId,
                Comments = issueComm
            };
        }

        [HttpPost("PostComment")]
        public async Task<ActionResult> CreateComment(IssueCommentDTO issueCommentDto)
        {
            var issueComm = _mapper.Map<IssueComment>(issueCommentDto);
            await _context.IssueComments.AddAsync(issueComm);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }
        #endregion
    }
}
