﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IssueTrackerService.Models
{
    [Table("issue")]
    public class Issue : BaseEntity
    {
        [Key]
        [Column("issue_id")]
        public int IssueId { get; set; }
        [Column("project_id")]
        public int ProjectId { get; set; }
        [Column("title")]
        public string Title { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [Column("priority")]
        public string Priority { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("assigned_to")]
        public int AssignedTo { get; set; }
        [Column("reported_by")]
        public int ReportedBy { get; set; }
        [Column("date_resolved")]
        public DateTime? DateResolved { get; set; }
    }
}
