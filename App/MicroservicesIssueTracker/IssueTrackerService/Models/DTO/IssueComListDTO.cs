﻿namespace IssueTrackerService.Models.DTO
{
    public class IssueComListDTO
    {
        public int IssueId { get; set; }
        public List<IssueComment> Comments { get; set; }
    }
}
