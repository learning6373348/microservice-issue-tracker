﻿namespace IssueTrackerService.Models.DTO
{
    public class IssueAssigneeDTO
    {
        public int AssignedTo { get; set; }
    }
}
