﻿namespace IssueTrackerService.Models.DTO
{
    public class IssueStatusDTO
    {
        public int Status { get; set; }
    }
}
