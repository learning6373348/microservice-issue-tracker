﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IssueTrackerService.Models
{
    [Table("issue_comments")]
    public class IssueComment : BaseEntity
    {
        [Key]
        [Column("comment_id")]
        public int CommentId { get; set; }
        [Column("issue_id")]
        public int IssueId { get; set; }
        [Column("comment")]
        public string? Comment { get; set; }
        [Column("user_id")]
        public int UserId { get; set; }
    }
}
