﻿using AutoMapper;
using IssueTrackerService.Models;
using IssueTrackerService.Models.DTO;
using System.Reflection;

namespace IssueTrackerService.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());
            CreateMap<IssueDTO, Issue>()
                .ForMember(d => d.Status, d => d.MapFrom(x => 0))
                .ForMember(d => d.AssignedTo, d => d.MapFrom(x => 0));
            CreateMap<IssueCommentDTO, IssueComment>();

        }
        private void ApplyMappingsFromAssembly(Assembly assembly)
        {
            var types = assembly.GetExportedTypes()
            .Where(t => t.GetInterfaces().Any(i =>
                    i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>)))
                .ToList();

            foreach (var type in types)
            {
                var instance = Activator.CreateInstance(type);

                var methodInfo = type.GetMethod("Mapping")
                    ?? type.GetInterface("IMapFrom`1").GetMethod("Mapping");

                methodInfo?.Invoke(instance, new object[] { this });

            }
        }
    }
}
