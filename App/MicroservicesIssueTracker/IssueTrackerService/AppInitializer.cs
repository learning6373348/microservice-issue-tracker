﻿using System.Runtime.CompilerServices;

namespace IssueTrackerService
{
    public static class AppInitializer
    {
        [ModuleInitializer]
        public static void Initialize()
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }
    }

}
